// **********************Section Our Services****************Start

const serviceContent = [];

for (let i = 0; i < 6; i++) {
    serviceContent[i] =
        {
            button: document.getElementsByClassName("our-services-buttons-item")[i],
            img: document.getElementsByClassName("our-services-image")[i],
            text: document.getElementsByClassName("our-services-text")[i]
        }
}

function clearActiveStatus() {
    document.querySelector('.active-button').classList.remove("active-button");
    let content=document.querySelectorAll('.active-content');
    content[0].classList.remove("active-content");
    content[1].classList.remove("active-content");
}

for (let item of serviceContent) {
    item.button.addEventListener("click", (event) => {
        clearActiveStatus();
        event.target.classList.add("active-button");
        for (let i = 0; i < 6; i++) {
            if (serviceContent[i].img.classList.contains(event.target.classList[1])) {
                serviceContent[i].img.classList.add("active-content");
            }
            if (serviceContent[i].text.classList.contains(event.target.classList[1])) {
                serviceContent[i].text.classList.add("active-content");
            }
        }
    })
}
// **********************Section Our Services**************** End

// **********************Section Our WORK**************** START
const ourWorkImageContainer = document.getElementsByClassName("our-work-images")[0];
const loadMoreButton = document.getElementsByClassName("load-more-button")[0];
// 5 Categories.
const CATEGORIES = [
    'our-work-graphic-design',
    'our-work-web-design',
    'our-work-landing-pages',
    'our-work-wordpress',
    'our-work-all'
];
//Tabs content designed to keep all data (img-url , info, header ,category);
//It simulates getting information from server.

const tabsContent = [
    new SingleTab('img/graphic design/graphic-design1.jpg',
        'Creative Design',
        "Graphic Design",
        CATEGORIES[0]
    ),
    new SingleTab('img/graphic design/graphic-design2.jpg',
        'Creative Design',
        "Graphic Design",
        CATEGORIES[0]
    ),
    new SingleTab('img/graphic design/graphic-design3.jpg',
        'Creative Design',
        "Graphic Design",
        CATEGORIES[0]
    ),
    new SingleTab('img/graphic design/graphic-design4.jpg',
        'Creative Design',
        "Graphic Design",
        CATEGORIES[0]
    ),
    new SingleTab('img/graphic design/graphic-design5.jpg',
        'Creative Design',
        "Graphic Design",
        CATEGORIES[0]
    ),
    new SingleTab('img/graphic design/graphic-design6.jpg',
        'Creative Design',
        "Graphic Design",
        CATEGORIES[0]
    ),
    new SingleTab('img/graphic design/graphic-design7.jpg',
        'Creative Design',
        "Graphic Design",
        CATEGORIES[0]
    ),
    new SingleTab('img/graphic design/graphic-design8.jpg',
        'Creative Design',
        "Graphic Design",
        CATEGORIES[0]
    ),
    new SingleTab('img/graphic design/graphic-design9.jpg',
        'Creative Design',
        "Graphic Design",
        CATEGORIES[0]
    ),
    new SingleTab('img/graphic design/graphic-design10.jpg',
        'Creative Design',
        "Graphic Design",
        CATEGORIES[0]
    ),
    new SingleTab('img/graphic design/graphic-design11.jpg',
        'Creative Design',
        "Graphic Design",
        CATEGORIES[0]
    ),
    new SingleTab('img/graphic design/graphic-design12.jpg',
        'Creative Design',
        "Graphic Design",
        CATEGORIES[0]
    ),
    new SingleTab('img/web design/web-design1.jpg',
        'Creative Design',
        "Web Design",
        CATEGORIES[1]
    ),
    new SingleTab('img/web design/web-design2.jpg',
        'Creative Design',
        "Web Design",
        CATEGORIES[1]
    ),
    new SingleTab('img/web design/web-design3.jpg',
        'Creative Design',
        "Web Design",
        CATEGORIES[1]
    ),
    new SingleTab('img/web design/web-design4.jpg',
        'Creative Design',
        "Web Design",
        CATEGORIES[1]
    ),
    new SingleTab('img/web design/web-design5.jpg',
        'Creative Design',
        "Web Design",
        CATEGORIES[1]
    ),
    new SingleTab('img/web design/web-design6.jpg',
        'Creative Design',
        "Web Design",
        CATEGORIES[1]
    ),
    new SingleTab('img/web design/web-design7.jpg',
        'Creative Design',
        "Web Design",
        CATEGORIES[1]
    ),
    new SingleTab('img/landing page/landing-page1.jpg',
        'Creative Design',
        "Landing Page",
        CATEGORIES[2]
    ),
    new SingleTab('img/landing page/landing-page2.jpg',
        'Creative Design',
        "Landing Page",
        CATEGORIES[2]
    ),
    new SingleTab('img/landing page/landing-page3.jpg',
        'Creative Design',
        "Landing Page",
        CATEGORIES[2]
    ),
    new SingleTab('img/landing page/landing-page4.jpg',
        'Creative Design',
        "Landing Page",
        CATEGORIES[2]
    ),
    new SingleTab('img/landing page/landing-page5.jpg',
        'Creative Design',
        "Landing Page",
        CATEGORIES[2]
    ),
    new SingleTab('img/landing page/landing-page6.jpg',
        'Creative Design',
        "Landing Page",
        CATEGORIES[2]
    ),
    new SingleTab('img/landing page/landing-page7.jpg',
        'Creative Design',
        "Landing Page",
        CATEGORIES[2]
    ),
    new SingleTab('img/wordpress/wordpress1.jpg',
        'Creative Design',
        "Wordpress",
        CATEGORIES[3]
    ),
    new SingleTab('img/wordpress/wordpress2.jpg',
        'Creative Design',
        "Wordpress",
        CATEGORIES[3]
    ),
    new SingleTab('img/wordpress/wordpress3.jpg',
        'Creative Design',
        "Wordpress",
        CATEGORIES[3]
    ),
    new SingleTab('img/wordpress/wordpress4.jpg',
        'Creative Design',
        "Wordpress",
        CATEGORIES[3]
    ),
    new SingleTab('img/wordpress/wordpress5.jpg',
        'Creative Design',
        "Wordpress",
        CATEGORIES[3]
    ),
    new SingleTab('img/wordpress/wordpress6.jpg',
        'Creative Design',
        "Wordpress",
        CATEGORIES[3]
    ),
    new SingleTab('img/wordpress/wordpress7.jpg',
        'Creative Design',
        "Wordpress",
        CATEGORIES[3]
    ),
    new SingleTab('img/wordpress/wordpress8.jpg',
        'Creative Design',
        "Wordpress",
        CATEGORIES[3]
    ),
    new SingleTab('img/wordpress/wordpress9.jpg',
        'Creative Design',
        "Wordpress",
        CATEGORIES[3]
    ),
    new SingleTab('img/wordpress/wordpress10.jpg',
        'Creative Design',
        "Wordpress",
        CATEGORIES[3]
    ),
];
//Each picture box is created with SingleTab function
function SingleTab(imgUrl, header, info, category = 'no category') {
    this.imgUrl = imgUrl;
    this.header = header;
    this.info = info;
    this.category = category;
    this.imageBox = document.createElement("div");
    this.imageBoxFocus = document.createElement("div");
    this.imageBoxFocus.classList.add("container-focus");
    //showTab function is designed to put element itself inside specific html container.
    //It will be called from ShowContent function
    this.showTab = function () {
        this.imageBox.style.cssText = `background-image: url("${this.imgUrl}");`;
        this.imageBox.classList.add("our-work-images-item");
        this.imageBoxFocus.innerHTML = "<div class=\"container-focus-icons\">\n" +
            "<img class=\"container-focus-icons-1\" src=\"img/icons/icon-work-1.png\" alt=\"icon\">\n" +
            "<img class=\"container-focus-icons-2\" src=\"img/icons/icon-work-2.png\" alt=\"icon\">\n" +
            "</div>\n" +
            "<h3 class=\"container-focus-header\"></h3>\n" +
            "<p class=\"container-focus-text\"></p>";
        this.imageBox.appendChild(this.imageBoxFocus);
        this.imageBoxFocus.style.display = "none";
        ourWorkImageContainer.appendChild(this.imageBox);
        this.imageBoxFocus.children[1].innerText = `${this.header}`;
        this.imageBoxFocus.children[2].innerText = `${this.info}`;
        this.imageBox.addEventListener("mouseenter", () => {
            this.imageBox.style.cssText = `background-image: none`;
            this.imageBoxFocus.style.display = "flex";
        });
        this.imageBox.addEventListener("mouseleave", () => {

            this.imageBoxFocus.style.display = "none";
            this.imageBox.style.cssText = `background-image: url("${this.imgUrl}");`;
        })
    };
}
//When pickContent ToShow finishes filtering it calls th Show content function
// with the filtered array to show
function ShowContent() {
    let counter = 0;
    for (let tab of tabsContent) {
        if (tab.category === activeCategory || activeCategory === "our-work-all") {
            if (counter < contentCount) {

                tab.showTab();
            }
            counter++;
        }
    }

    if (counter <= contentCount) {
        loadMoreButton.style.display = "none";
        ourWorkImageContainer.style.paddingBottom = "100px";
    }
}
//Before Showcontent function    pickContentToShow filters the array with class names
// to choose required contents for the specific category

//It is called On category click events
function pickContentToShow() {
    if (contentCount === 36) {
        loadMoreButton.style.display = "none";
        ourWorkImageContainer.style.paddingBottom = "100px";
    } else {
        loadMoreButton.style.display = "flex";
        ourWorkImageContainer.style.paddingBottom = "0";
    }
    document.getElementsByClassName("our-work-buttons-active")[0].classList.remove("our-work-buttons-active");
    let button = document.getElementsByClassName(activeCategory)[0];
    button.classList.add("our-work-buttons-active");
    ourWorkImageContainer.innerHTML = "";
    tabsContent.sort(() => Math.random() - 0.5);
    ShowContent();
}
//It is default value when the page loads.
//Category: All
//Content Count: 12
let activeCategory = CATEGORIES[4];
let contentCount = 12;
ShowContent();
//-----------------------------

//Creating click event for Each category button
for (let i = 0; i < CATEGORIES.length; i++) {
    document.getElementsByClassName(CATEGORIES[i])[0].addEventListener("click", () => {
        contentCount = 12;
        activeCategory = CATEGORIES[i];
        pickContentToShow()
    });
}

//Click event for LOad More images button
loadMoreButton.addEventListener("click", () => {
    if (contentCount < 36) {
        contentCount += 12;
        pickContentToShow();
    }
});

// **********************Section Our WORK**************** End

// **********************Section About ham**************** Start
//Arrow Buttons
const left = document.querySelector('.left');
const right = document.querySelector('.right');
//Main big carousel
const slider = document.querySelector('.carousel-slider');


const indicatorParent = document.querySelector('.indicators');
const indicators = document.querySelectorAll('.indicators-item');

//Index is used to:
// 1) keep track of required translateX value
// 2) Make main slider compatible with indicators
let sliderIndex = 0;

//This function is designed to accept sepcific button(right or left),
// and its state(true or false); It turns gives hover effect when needed
function arrowSwtich(arrow,check,turnOff = false) {
    if(turnOff){
        arrow.style.cssText = "background-color: none;" +
            " color: #7f898c;" +
            "border: 1px solid #7f898c;";
        return;
    }

    console.log(arrow)
    if(arrow===left)
    {
        if (sliderIndex > 0) {
            if(check){
                sliderIndex--;
            }

            arrow.style.cssText = "background-color: #18cfab;" +
                "color: white;" +
                "border: none;";
        } else {
            if(check) {
                sliderIndex = 0;
            }
            arrow.style.cssText = "background-color: none;" +
                " color: #7f898c;" +
                "border: 1px solid #7f898c;";
        }
    }
    else if(arrow===right){
        if (sliderIndex < 3) {
            if(check) {
                sliderIndex++;
            }
            arrow.style.cssText = "background-color: #18cfab;" +
                "color: white;" +
                "border: none;";
        } else {
            if(check) {
                sliderIndex = 3;
            }
            arrow.style.cssText = "background-color: none;" +
                " color: #7f898c;" +
                "border: 1px solid #7f898c;";
        }
    }
}

//Creating click events for each picture indicators

indicators.forEach((indicator, i) => {
    indicator.addEventListener('click', () => {
        document.querySelector('.control .indicators-selected').classList.remove('indicators-selected');
        indicator.classList.add('indicators-selected');
        slider.style.transform = 'translateX(' + (i) * -25 + '%)';
        sliderIndex = i;
    });
});

function arrowClick(event){
    arrowSwtich(event.target,true,false);
    document.querySelector('.control .indicators-selected').classList.remove('indicators-selected');
    indicatorParent.children[sliderIndex].classList.add('indicators-selected');
    slider.style.transform = 'translateX(' + (sliderIndex) * -25 + '%)';
    arrowSwtich(event.target,false,false);
}

left.addEventListener('click', arrowClick);
right.addEventListener('click', arrowClick);



right.addEventListener("mouseover", () => {
    arrowSwtich(right,false,false);
});
right.addEventListener("mouseout", () => {
    arrowSwtich(right, false,true);
});


left.addEventListener("mouseover", () => {
    debugger;
    arrowSwtich(left,false,false);
});
left.addEventListener("mouseout", () => {
    arrowSwtich(left, false,true);
});

// **********************Section  About ham**************** End